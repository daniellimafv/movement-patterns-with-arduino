#include <Arduino.h>
#include "Wire.h" // This library allows you to communicate with I2C devices.

const int MPU_ADDR = 0x68; // I2C address of the MPU-6050. If AD0 pin is set to HIGH, the I2C address will be 0x69.

int16_t accelerometer_x, accelerometer_y, accelerometer_z; // variables for accelerometer raw data

int16_t gyro_x, gyro_y, gyro_z; // variables for gyro raw data

int16_t temperature; // variables for temperature data

char tmp_str[7]; // temporary variable used in convert function

int minVal = 265;
int maxVal = 402;

double x;
double y;
double z;

double ac_x_array[30];
double ac_y_array[30];
double ac_z_array[30];

char* convert_int16_to_str(int16_t i) { // converts int16 to string. Moreover, resulting strings will have the same length in the debug monitor.
  sprintf(tmp_str, "%6d", i);
  return tmp_str;
}

int calc_array_size(double array[]) {
  int size = 0;
  for(int i = 0; i <= 29; i++) {
      if (array[i] != 0) {
        size = size + 1;
      }
    }
  return size;
}

double calc_array_avarage(double array[], int size) {
  double avarage = 0;
  
  for(int i = 0; i <= (size - 1); i++) {
    avarage = avarage + array[i];
  }

  return avarage / 30;
}

double avg_ac(double i, double array[]){
  int size = calc_array_size(array);

  if (size <= 29) {
    array[size] = i;
    return i;
  }
  else
  {
    for(int i = 1; i <= (size - 1); i++) {
      array[i - 1] = array[i];
    }
    array[29] = i;

    return calc_array_avarage(array, size);
  }
}

void setup() {
  Serial.begin(9600);
  Wire.begin();
  Wire.beginTransmission(MPU_ADDR); // Begins a transmission to the I2C slave (GY-521 board)
  Wire.write(0x6B); // PWR_MGMT_1 register
  Wire.write(0); // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
}

void loop() {
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H) [MPU-6000 and MPU-6050 Register Map and Descriptions Revision 4.2, p.40]
  Wire.endTransmission(false); // the parameter indicates that the Arduino will send a restart. As a result, the connection is kept active.
  Wire.requestFrom(MPU_ADDR, 7*2, true); // request a total of 7*2=14 registers
  
  // "Wire.read()<<8 | Wire.read();" means two registers are read and stored in the same variable
  accelerometer_x = Wire.read()<<8 | Wire.read(); // reading registers: 0x3B (ACCEL_XOUT_H) and 0x3C (ACCEL_XOUT_L)
  accelerometer_y = Wire.read()<<8 | Wire.read(); // reading registers: 0x3D (ACCEL_YOUT_H) and 0x3E (ACCEL_YOUT_L)
  accelerometer_z = Wire.read()<<8 | Wire.read(); // reading registers: 0x3F (ACCEL_ZOUT_H) and 0x40 (ACCEL_ZOUT_L)
  temperature = Wire.read()<<8 | Wire.read(); // reading registers: 0x41 (TEMP_OUT_H) and 0x42 (TEMP_OUT_L)
  gyro_x = Wire.read()<<8 | Wire.read(); // reading registers: 0x43 (GYRO_XOUT_H) and 0x44 (GYRO_XOUT_L)
  gyro_y = Wire.read()<<8 | Wire.read(); // reading registers: 0x45 (GYRO_YOUT_H) and 0x46 (GYRO_YOUT_L)
  gyro_z = Wire.read()<<8 | Wire.read(); // reading registers: 0x47 (GYRO_ZOUT_H) and 0x48 (GYRO_ZOUT_L)
  
  //Convert Acc in angles
  int xAng = map(accelerometer_x,minVal,maxVal,-90,90);
  int yAng = map(accelerometer_y,minVal,maxVal,-90,90);
  int zAng = map(accelerometer_z,minVal,maxVal,-90,90);
  x = RAD_TO_DEG * (atan2(-yAng, -zAng) + PI);
  y = RAD_TO_DEG * (atan2(-xAng, -zAng) + PI);
  z = RAD_TO_DEG * (atan2(-yAng, -xAng) + PI);

  // print out data
  Serial.print("aX = "); Serial.print(avg_ac(x, ac_x_array));
  Serial.print(" | aY = "); Serial.print(avg_ac(y, ac_y_array));
  Serial.print(" | aZ = "); Serial.print(avg_ac(z, ac_z_array));
  // the following equation was taken from the documentation [MPU-6000/MPU-6050 Register Map and Description, p.30]
  Serial.print(" | tmp = "); Serial.print(temperature/340.00+36.53);
  Serial.print(" | gX = "); Serial.print(convert_int16_to_str(gyro_x));
  Serial.print(" | gY = "); Serial.print(convert_int16_to_str(gyro_y));
  Serial.print(" | gZ = "); Serial.print(convert_int16_to_str(gyro_z));
  Serial.println();
  
  // delay
  //delay(300);
}